<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\components\gii;

use yii\gii\generators\crud\Generator;
use yii\helpers\Inflector;

/**
 * Description of CrudGenerator
 *
 * @author Rust
 */
class CrudGenerator extends Generator {
	
	public function generateSearchLabels()
    {
        /* @var $model \yii\base\Model */
        //$model = new $this->modelClass();
		$class = $this->modelClass;
		$model = $class::instance();
        $attributeLabels = $model->attributeLabels();
        $labels = [];
        foreach ($this->getColumnNames() as $name) {
            if (isset($attributeLabels[$name])) {
                $labels[$name] = $attributeLabels[$name];
            } else {
                if (!strcasecmp($name, 'id')) {
                    $labels[$name] = 'ID';
                } else {
                    $label = Inflector::camel2words($name);
                    if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
                        $label = substr($label, 0, -3) . ' ID';
                    }
                    $labels[$name] = $label;
                }
            }
        }

        return $labels;
    }
	
}
