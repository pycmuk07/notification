<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\components\helpers;

use yii\helpers\Inflector as BaseInflector;

/**
 * Description of Inflector
 *
 * @author Rust
 */
class Inflector extends BaseInflector {
	
	public static function camel2id($name, $separator = '-', $strict = false)
    {
		$res = '';
		if (mb_strlen($name) > 0) {
			for ($i = 0; $i < mb_strlen($name); $i++) {
				$char = mb_substr($name, $i, 1);
				if ($char !== mb_strtolower($char)) {
					$res .= $separator . mb_strtolower($char);
					continue;
				}
				$res .= $char;
			}
		}
		return $res;
    }
	
}
