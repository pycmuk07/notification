<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\components\mailer;

use yii\swiftmailer\Message as BaseMessage;

/**
 * Description of Message
 *
 * @author Rust
 */
class Message extends BaseMessage {
	
	public function setHtmlBody($html)
    {
		$preg = '#<img .*?>#';
		preg_match_all($preg, $html, $found);
		
		if (count($found) == 0 || count($found[0]) == 0) {
			return parent::setHtmlBody($html);
		}
		
		$found = implode(' ', $found[0]);
		$preg = '#src=([^ ]+)? #';
		
		preg_match_all($preg, $found, $match);
		
		if (count($match) < 2 || count($match[1]) == 0) {
			return parent::setHtmlBody($html);
		}

		$search = [];
		$replace = [];
		foreach ($match[1] as $url) {
			$search[] = $url;
			$url = str_replace('"', '', $url);
			$url = str_replace("'", '', $url);
			$url = str_replace("http://notif.de", \Yii::getAlias('@webroot'), $url);
			$replace[] = $this->embed($url);
		}
		
		$html = str_replace($search, $replace, $html);

		return parent::setHtmlBody($html);
    }
	
}
