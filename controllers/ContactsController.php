<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use domain\entities\Contact\Contact;
use domain\entities\Contact\ContactSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use domain\services\ContactService;
use domain\services\FilterService;
use domain\forms\Contact\ContactForm;

/**
 * ContactsController implements the CRUD actions for Contact model.
 */
class ContactsController extends Controller
{
	
	private $contactService;
	private $filterService;
	
	public function __construct($id, $module, ContactService $contactService, FilterService $filterService, $config = array()) {
		parent::__construct($id, $module, $config);
		$this->contactService = $contactService;
		$this->filterService = $filterService;
	}
	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contact model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		$form = $this->contactService->findById($id);
        return $this->render('view', [
            'model' => $form,
        ]);
    }

    /**
     * Creates a new Contact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new ContactForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$id = $this->contactService->create($form);
				return $this->redirect(['view', 'id' => $id]);
			} catch (\Exception $e) {
				Yii::$app->session->setFlash('domainError', $e);
			}
        }

		$filters = ArrayHelper::map($this->filterService->all(), 'id', 'title');
        return $this->render('create', [
            'model' => $form,
			'filters' => $filters
        ]);
    }

    /**
     * Updates an existing Contact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $form = $this->contactService->findById($id);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$id = $this->contactService->edit($form);
				return $this->redirect(['view', 'id' => $id]);
			} catch (\Exception $e) {
				Yii::$app->session->setFlash('domainError', $e);
			}
        }
		
		$filters = ArrayHelper::map($this->filterService->all(), 'id', 'title');
        return $this->render('update', [
            'model' => $form,
			'filters' => $filters
        ]);
    }

    /**
     * Deletes an existing Contact model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
		$this->contactService->deleteById($id);
        return $this->redirect(['index']);
    }
}
