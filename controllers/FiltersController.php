<?php

namespace app\controllers;

use Yii;
use domain\entities\Filter\Filter;
use domain\entities\Filter\FilterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use domain\services\FilterService;
use domain\forms\FilterForm;
use domain\exceptions\CantSaveException;

/**
 * FiltersController implements the CRUD actions for Filter model.
 */
class FiltersController extends Controller
{
	
	/**
	 *
	 * @var FilterService 
	 */
	private $filterService;
	
	/**
	 * 
	 * @param FilterService $service
	 * @param type $id
	 * @param type $module
	 * @param type $config
	 */
	public function __construct($id, $module, FilterService $service, $config = array()) {
		parent::__construct($id, $module, $config);
		$this->filterService = $service;
	}
	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Filter models.
     * @return mixed
     */
    public function actionIndex()
    {	
        $searchModel = new FilterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Filter model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		$form = $this->filterService->findById($id);
        return $this->render('view', [
            'model' => $form,
        ]);
    }

    /**
     * Creates a new Filter model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new FilterForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$id = $this->filterService->create($form);
				return $this->redirect(['view', 'id' => $id]);
			} catch (CantSaveException $exception) {
				Yii::$app->session->setFlash('domainError', $exception);
			}
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * Updates an existing Filter model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $form = $this->filterService->findById($id);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
				$id = $this->filterService->edit($form);
				return $this->redirect(['view', 'id' => $id]);
			} catch (CantSaveException $exception) {
				Yii::$app->session->setFlash('domainError', $exception);
			}
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    /**
     * Deletes an existing Filter model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->filterService->deleteById($id);
        return $this->redirect(['index']);
    }

}
