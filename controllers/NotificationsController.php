<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use domain\entities\Notification\Notification;
use domain\entities\Notification\NotificationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use domain\services\NotificationService;
use domain\services\FilterService;
use domain\forms\Notification\NotificationForm;
use domain\services\NotificationDispatcher;

/**
 * NotificationsController implements the CRUD actions for Notification model.
 */
class NotificationsController extends Controller
{
	
	protected $notificationService;
	protected $filterService;
	protected $notificationDispatcher;


	public function __construct(
			$id, 
			$module, 
			NotificationService $notificationService, 
			FilterService $filterService,
			NotificationDispatcher $notificationDispatcher,
			$config = array()
		) {
		parent::__construct($id, $module, $config);
		$this->notificationService = $notificationService;
		$this->filterService = $filterService;
		$this->notificationDispatcher = $notificationDispatcher;
	}

	public function actions() {
		$actions = parent::actions();
		return ArrayHelper::merge($actions, 
				[
					'file-upload' => [
						'class' => 'vova07\imperavi\actions\UploadFileAction',
						'url' => Yii::$app->urlManager->createAbsoluteUrl(['/uploads']), // Directory URL address, where files are stored.
						'path' => '@app/web/uploads', // Or absolute path to directory where files are stored.
						'uploadOnlyImage' => false, // For any kind of files uploading.
					],
				]
		);
	}
	
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notification model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
	$form = $this->notificationService->findById($id);
        return $this->render('view', [
            'model' => $form,
        ]);
    }

    /**
     * Creates a new Notification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new NotificationForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$id = $this->notificationService->create($form);
				return $this->redirect(['view', 'id' => $id]);
			} catch (\Exception $e) {
				Yii::$app->session->setFlash('domainError', $e);
			}
        }

	$filters = ArrayHelper::map($this->filterService->all(), 'id', 'title');
        return $this->render('create', [
            'model' => $form,
            'filters' => $filters
        ]);
    }

    /**
     * Updates an existing Notification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $form = $this->notificationService->findById($id);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$id = $this->notificationService->edit($form);
				return $this->redirect(['view', 'id' => $id]);
			} catch (\Exception $e) {
				Yii::$app->session->setFlash('domainError', $e);
			}
        }

		$filters = ArrayHelper::map($this->filterService->all(), 'id', 'title');
        return $this->render('update', [
            'model' => $form,
			'filters' => $filters
        ]);
    }

    /**
     * Deletes an existing Notification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->notificationService->deleteById($id);

        return $this->redirect(['index']);
    }

	public function actionSend($id) {
		try {
			$this->notificationDispatcher->notifyById($id);
			Yii::$app->session->setFlash('successNotification', true);
		} catch (\Exception $e) {
			Yii::$app->session->setFlash('domainError', $e);
		}
		return $this->actionIndex();
	}
	
}
