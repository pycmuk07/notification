<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\bootstrap;

use Yii;
use yii\base\BootstrapInterface;

use domain\services\mailing\SmsService;

/**
 * Description of Bootstrap
 *
 * @author Rust
 */
class Bootstrap implements BootstrapInterface {
	
	public function bootstrap($app) {
		Yii::$container->setSingleton(SmsService::class, [
			'url' => 'http://sms.ru/sms/send',									//адрес шлюза
			'api' => '8de7e2ca-9b10-0294-f1d6-23a123e4c4ef',					//логин
		]);
	}

}
