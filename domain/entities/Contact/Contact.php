<?php

namespace domain\entities\Contact;

use Yii;

use domain\traits\InstantiateTrait;
use domain\entities\Filter\Filter;
use domain\entities\Contact\ContactFilter;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $addressname
 * @property string $client_name
 * @property string $phone
 * @property string $email
 * @property string $sigment1
 * @property string $sigment2
 *
 * @property ContactsFilters[] $contactsFilters
 * @property Filters[] $filters
 */
class Contact extends \yii\db\ActiveRecord
{
	
	use InstantiateTrait;
	
	public function __construct($addressname, $client_name, $config = array()) {
		parent::__construct($config);
		$this->addressname = $addressname;
		$this->client_name = $client_name;
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

	public static function find() {
		return new ContactQuery(get_called_class());
	}
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactFilters()
    {
        return $this->hasMany(ContactFilter::className(), ['contact_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilters()
    {
        return $this->hasMany(Filter::className(), ['id' => 'filter_id'])
				->via('contactFilters');
    }
}
