<?php

namespace domain\entities\Contact;

use Yii;

use domain\entities\Filter\Filter;
use domain\traits\InstantiateTrait;

/**
 * This is the model class for table "contacts_filters".
 *
 * @property int $id
 * @property int $contact_id
 * @property int $filter_id
 *
 * @property Filters $filter
 * @property Contacts $contact
 */
class ContactFilter extends \yii\db\ActiveRecord
{
	use InstantiateTrait;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts_filters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'filter_id'], 'required'],
            [['contact_id', 'filter_id'], 'integer'],
            [['contact_id', 'filter_id'], 'unique', 'targetAttribute' => ['contact_id', 'filter_id']],
            [['filter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filter::className(), 'targetAttribute' => ['filter_id' => 'id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['contact_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contact_id' => 'Contact ID',
            'filter_id' => 'Filter ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilter()
    {
        return $this->hasOne(Filters::className(), ['id' => 'filter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }
}
