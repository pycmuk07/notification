<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\entities\Contact;

use yii\db\ActiveQuery;
use domain\entities\Contact\ContactFilter;

/**
 * Description of ContactQuery
 *
 * @author Rust
 */
class ContactQuery extends ActiveQuery {
	
	public function one($db = null) {
		return parent::one($db);
	}
	
	public function all($db = null) {
		return parent::all($db);
	}
	
	public function byFilterIds($filterId) {
		$nft = ContactFilter::tableName();
		return $this->joinWith('contactFilters')
			->andWhere(["$nft.filter_id" => $filterId]);
	}
	
}
