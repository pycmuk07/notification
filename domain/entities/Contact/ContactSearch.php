<?php

namespace domain\entities\Contact;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use domain\entities\Contact\Contact;

/**
 * ContactSearch represents the model behind the search form of `domain\entities\Contact\Contact`.
 */
class ContactSearch extends Model
{
	public $id;
	public $addressname;
	public $client_name;
	public $phone;
	public $email;
	public $sigment1;
	public $sigment2;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['addressname', 'client_name', 'phone', 'email', 'sigment1', 'sigment2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contact::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'addressname', $this->addressname])
            ->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'sigment1', $this->sigment1])
            ->andFilterWhere(['like', 'sigment2', $this->sigment2]);

        return $dataProvider;
    }
}
