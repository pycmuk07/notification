<?php

namespace domain\entities\Filter;

use Yii;
use domain\traits\InstantiateTrait;
/**
 * This is the model class for table "filters".
 *
 * @property int $id
 * @property string $title Название фильтра
 * @property string $description Описание
 *
 * @property ContactsFilters[] $contactsFilters
 * @property Contacts[] $contacts
 * @property NotificationsFilters[] $notificationsFilters
 */
class Filter extends \yii\db\ActiveRecord
{
	use InstantiateTrait;
	
	public function __construct($title, $description = null, $config = array()) {
		parent::__construct($config);
		$this->title = $title;
		$this->description = $description;
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filters';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsFilters()
    {
        return $this->hasMany(ContactsFilters::className(), ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contacts::className(), ['id' => 'contact_id'])->viaTable('contacts_filters', ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationsFilters()
    {
        return $this->hasMany(NotificationsFilters::className(), ['filter_id' => 'id']);
    }
}
