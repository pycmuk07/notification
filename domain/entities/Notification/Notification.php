<?php

namespace domain\entities\Notification;

use Yii;

use domain\traits\InstantiateTrait;
use domain\entities\Filter\Filter;

/**
 * This is the model class for table "notifications".
 *
 * @property int $id
 * @property string $title Название рассылки
 * @property string $heading Заголовок сообщения
 * @property string $content Тело сообщения
 * @property int $email
 * @property int $sms
 *
 * @property NotificationsFilters[] $notificationsFilters
 */
class Notification extends \yii\db\ActiveRecord
{
	use InstantiateTrait;
	
	public function __construct($title, $config = array()) {
		parent::__construct($config);
		$this->title = $title;
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

	public static function find() {
		return new NotificationQuery(get_called_class());
	}
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilters()
    {
        return $this->hasMany(Filter::class, ['id' => 'filter_id'])
			->via('notificationFilters');
    }
	
	public function getNotificationFilters() {
		return $this->hasMany(NotificationFilter::class, ['notification_id' => 'id']);
	}
	
}
