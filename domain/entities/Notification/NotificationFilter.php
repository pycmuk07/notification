<?php

namespace domain\entities\Notification;

use Yii;

use domain\traits\InstantiateTrait;
use domain\entities\Filter\Filter;


/**
 * This is the model class for table "notifications_filters".
 *
 * @property int $id
 * @property int $notification_id
 * @property int $filter_id
 *
 * @property Filters $filter
 * @property Notifications $notification
 */
class NotificationFilter extends \yii\db\ActiveRecord
{
	
	use InstantiateTrait;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications_filters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_id', 'filter_id'], 'required'],
            [['notification_id', 'filter_id'], 'integer'],
            [['filter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filter::className(), 'targetAttribute' => ['filter_id' => 'id']],
            [['notification_id'], 'exist', 'skipOnError' => true, 'targetClass' => Notification::className(), 'targetAttribute' => ['notification_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notification_id' => 'Notification ID',
            'filter_id' => 'Filter ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilter()
    {
        return $this->hasOne(Filter::className(), ['id' => 'filter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(Notification::className(), ['id' => 'notification_id']);
    }
}
