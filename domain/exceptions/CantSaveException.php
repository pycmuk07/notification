<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\exceptions;

use yii\base\Exception;

/**
 * Description of CantSaveException
 *
 * @author Rust
 */
class CantSaveException extends Exception {
	
	protected $message = 'Не удалось сохранить запись!';
	protected $model;
	
	public function __construct($message = "", $model = null, $code = 0, \Exception $previous = null) {
		parent::__construct($message, $code, $previous);
		if ($model != null) {
			$this->model = $model;
		}
	}
	
	public function getModel() {
		return $this->model;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getName()
    {
        return 'CantSaveException';
    }
	
}
