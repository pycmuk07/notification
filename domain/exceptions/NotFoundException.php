<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\exceptions;

use yii\base\Exception;

/**
 * Description of NotFoundException
 *
 * @author Rust
 */
class NotFoundException extends Exception {
	
	protected $message = 'Переменная не найдена';
	
	public function getName()
    {
        return 'NotFoundException';
    }
	
}
