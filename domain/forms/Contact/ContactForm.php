<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\forms\Contact;

use yii\base\Model;
use yii\helpers\ArrayHelper;

use domain\entities\Contact\Contact;
use domain\validators\FiltersValidator;

/**
 * Description of ContactForm
 *
 * @author Rust
 */
class ContactForm extends Model {
	
	private $_contact;
	
	public $id;
	public $addressname;
	public $client_name;
	public $phone;
	public $email;
	public $sigment1;
	public $sigment2;
	
	public $filters = [];
	
	public function __construct(Contact $contact = null, $config = array()) {
		parent::__construct($config);
		if ($contact !== null) {
			$this->_contact = $contact;
			$this->id = $contact->id;
			$this->addressname = $contact->addressname;
			$this->client_name = $contact->client_name;
			$this->phone = $contact->phone;
			$this->email = $contact->email;
			$this->sigment1 = $contact->sigment1;
			$this->sigment2 = $contact->sigment2;
			
			if (count($contact->contactFilters) > 0) {
				$this->filters = ArrayHelper::getColumn($contact->contactFilters, 'filter_id');
			}
			
		}
	}
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			['filters', FiltersValidator::class],
            [['addressname', 'client_name'], 'required'],
            [['addressname', 'sigment1', 'sigment2'], 'string'],
            [['client_name', 'email'], 'string', 'max' => 255],
            [['phone'], 'integer'],
			['email', 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'addressname' => 'Название контрагента',
            'client_name' => 'Контактное лицо',
            'phone' => 'Phone',
            'email' => 'Email',
            'sigment1' => 'Sigment1',
            'sigment2' => 'Sigment2',
        ];
    }
	
	public function getContactFilters() {
		return $this->_contact->filters;
	}
	
}
