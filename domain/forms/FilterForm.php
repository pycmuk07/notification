<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\forms;

use Yii;
use yii\base\Model;

use domain\entities\Filter\Filter;

/**
 * Description of FilterForm
 *
 * @author Rust
 */
class FilterForm extends Model {
	
	private $_filter;
	
	public $id;
	public $title;
	public $description;
	
	public function __construct(Filter $filter = null, $config = array()) {
		parent::__construct($config);
		if ($filter !== null) {
			$this->_filter = $filter;
			$this->id = $filter->id;
			$this->title = $filter->title;
			$this->description = $filter->description;
		}
	}
	
	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Описание',
        ];
    }
	
}
