<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\repositories;

use domain\entities\Contact\Contact;
use domain\entities\Contact\ContactFilter;
use domain\exceptions\NotFoundException;
use domain\exceptions\CantSaveException;
use domain\exceptions\CantDeleteException;
use domain\exceptions\IncorrectTypeException;

/**
 * Description of ContactRepository
 *
 * @author Rust
 */
class ContactRepository {
	
	/**
	 * 
	 * @param type $id
	 * @return Contact
	 * @throws NotFoundException
	 */
	public function get($id) {
		$contact = Contact::findOne(['id' => $id]);
		if ($contact === null) {
			throw new NotFoundException('Не удалось найти контакт по идентификатору ' . $id);
		}
		return $contact;
	}
	
	/**
	 * 
	 * @param integer[] $filterIds
	 * @return Contact[]
	 * @throws IncorrectTypeException
	 */
	public function getByFilterIds(array $filterIds) {
		$contacts = [];
		if (count($filterIds) > 0) {
			$ids = [];
			foreach ($filterIds as $key => $id) {
				if (!is_numeric($id)) {
					throw new IncorrectTypeException('Идентификатор фильтра должен быть целым числом!');
				}
				$ids[] = $id;
			}
			$contacts = Contact::find()->byFilterIds($ids)->all();
		}
		return $contacts;
	}
	
	/**
	 * 
	 * @param Contact $contact
	 * @return integer $id
	 * @throws CantSaveException
	 */
	public function save(Contact $contact) {
		try {
			if (!$contact->save()) {
				throw new CantSaveException('Не удалось сохранить контакт');
			}
		} catch (\Exception $e) {
			throw new CantSaveException(
				'Не удалось сохранить контакт',
				$contact,
				0,
				$e
			);
		}
		return $contact->id;
	}
	
	public function delete(Contact $contact) {
		try {
			if (!$contact->delete()) {
				throw new CantDeleteException('Не удалось удалить контакт с идентификатором ' . $contact->id);
			}
		} catch (\Exception $e) {
			throw new CantDeleteException(
				'Не удалось удалить контакт с идентификатором ' . $contact->id,
				$contact,
				0,
				$e
			);
		}
	}
	
	/**
	 * 
	 * @param Contact $contact
	 * @throws \Exception
	 * @throws CantDeleteException
	 */
	public function deleteFilters(Contact $contact) {
		$filters = $contact->contactFilters;
		if (count($filters) > 0) {
			foreach ($filters as $filter) {
				try {
					if (!$filter->delete()) {
						throw new CantDeleteException(
							'Не удалось открепить фильтр с идентификатором ' . $filter->filter_id
						);
					}
				} catch (\Exception $e) {
					throw $e;
				}
			}
		}
	}
	
	/**
	 * 
	 * @param Contact $contact
	 * @param type $filter_id
	 * @return integer $contact_filter_id
	 * @throws CantSaveException
	 */
	public function addFilter(Contact $contact, $filter_id) {
		$filter = ContactFilter::findOne([
			'contact_id' => $contact->id,
			'filter_id' => $filter_id
		]);
		if ($filter === null) {
			$filter = new ContactFilter();
			$filter->contact_id = $contact->id;
			$filter->filter_id = $filter_id;
		}
		
		try {
			if (!$filter->save()) {
				throw new CantSaveException('Не удалось добавить фильтр с идентификатором ' . $filter_id);
			}
		} catch (\Exception $e) {
			throw new CantSaveException(
				'Не удалось добавить фильтр с идентификатором ' . $filter_id,
				$contact,
				0,
				$e
			);
		}
		return $filter->id;
	}
	
}
