<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\repositories;

use domain\entities\Filter\Filter;
use domain\exceptions\CantSaveException;
use domain\exceptions\CantDeleteException;
use domain\exceptions\NotFoundException;
use domain\exceptions\IncorrectTypeException;

/**
 * Description of FilterRepository
 *
 * @author Rust
 */
class FilterRepository {
	
	/**
	 * 
	 * @param integer $id
	 * @return Filter
	 */
	public function get($id) {
		$filter = Filter::findOne(['id' => $id]);
		$this->ensureFound($filter, $id);
		return $filter;
	}
	
	/**
	 * 
	 * @return Filter[]
	 */
	public function getAll() {
		return Filter::find()->all();
	}
	
	/**
	 * 
	 * @param string $title
	 * @return Filter
	 */
	public function getByTitle(string $title) {
		$filter = Filter::findOne(['title' => $title]);
		$this->ensureFound($filter, $title);
		return $filter;
	}
	
	/**
	 * 
	 * @param string[] $titles
	 * @return Filter[]
	 * @throws IncorrectTypeException
	 */
	public function getByTitles(array $titles) {
		$filters = [];
		if (count($titles) > 0) {
			$titlesArray = [];
			foreach ($titles as $key => $title) {
				if (!is_string($title) && !is_numeric($title)) {
					throw new IncorrectTypeException('Название фильтра должно быть строкой!');
				}
				$titlesArray[] = $title;
			}
			$filters = Filter::findAll(['title' => $titlesArray]);
		}
		return $filters;
	}
	
	/**
	 * 
	 * @param Filter $filter
	 * @return integer $id
	 * @throws CantSaveException
	 */
	public function save(Filter $filter) {
		try {
			if (!$filter->save()) {
				throw new CantSaveException('Не удалось сохранить фильтр', $filter, 0, $e);
			}
		} catch (\Exception $e) {
			$exception = new CantSaveException('Не удалось сохранить фильтр', $filter, 0, $e);
			throw $exception;
		}
		return $filter->id;
	}
	
	/**
	 * 
	 * @param Filter $filter
	 * @throws CantDeleteException
	 */
	public function delete(Filter $filter) {
		try {
			$filter->delete();
		} catch (\Exception $e) {
			throw new CantDeleteException(
				'Не удалось удалить фильтр с идентификатором ' . $filter->id,
				$filter,
				0,
				$e
			);
		}
	}
	
	/**
	 * 
	 * @param array|integer $ids
	 * @throws NotFoundException
	 */
	public function ensureExists($ids) {
		if (is_array($ids)) {
			if (count($ids) > 0) {
				foreach ($ids as $key => $id) {
					$this->ensureExists($id);
				}
			}
		}
		else {
			try {
				$exists = $this->ifExists($ids);
				if (!$exists) {
					throw new NotFoundException('Не удалось найти фильтр по идентификатору ' . $ids);
				}
			} catch (\Exception $e) {
				throw new NotFoundException('Не удалось найти фильтр по идентификатору ' . $ids);
			}
		}
	}
	
	/**
	 * 
	 * @param integer $id
	 * @return boolean
	 */
	public function ifExists($id) {
		return Filter::find()->where(['id' => $id])->exists();
	}
	
	/**
	 * 
	 * @param Filter $filter
	 * @param integer|string $id
	 * @throws NotFoundException
	 */
	protected function ensureFound(Filter $filter, $id = null) {
		if ($filter == null) {
			throw new NotFoundException('Не удалось найти фильтр по идентификатору ' . $id);
		}
	}
	
}
