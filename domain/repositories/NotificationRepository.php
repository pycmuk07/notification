<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\repositories;

use domain\entities\Notification\Notification;
use domain\exceptions\NotFoundException;
use domain\exceptions\CantDeleteException;
use domain\exceptions\CantSaveException;
use domain\entities\Notification\NotificationFilter;

/**
 * Description of NotificationRepository
 *
 * @author Rust
 */
class NotificationRepository {
	
	/**
	 * 
	 * @param type $id
	 * @return Notification
	 * @throws NotFoundException
	 */
	public function get($id) {
		$notification = Notification::findOne(['id' => $id]);
		if ($notification === null) {
			throw new NotFoundException('Не удалось найти рассылку по идентификатору ' . $id);
		}
		return $notification;
	}
	
	/**
	 * 
	 * @param integer $filterId
	 * @return Notification[]
	 */
	public function getByFilterId($filterId) {
		return Notification::find()->byFilterId($filterId)->all();
	}
	
	public function getByTitles(array $titles) {
		return Notification::findAll(['title' => $titles]);
	}


	/**
	 * 
	 * @param Notification $notification
	 * @return integer $id
	 * @throws CantSaveException
	 */
	public function save(Notification $notification) {
		try {
			if (!$notification->save()) {
				throw new CantSaveException('Не удалось сохранить рассылку');
			}
		} catch (\Exception $e) {
			throw new CantSaveException(
				'Не удалось сохранить рассылку',
				$notification,
				0,
				$e
			);
		}
		return $notification->id;
	}
	
	public function delete(Notification $notification) {
		try {
			if (!$notification->delete()) {
				throw new CantDeleteException('Не удалось удалить рассылку с идентификатором ' . $notification->id);
			}
		} catch (\Exception $e) {
			throw new CantDeleteException(
				'Не удалось удалить рассылку с идентификатором ' . $notification->id,
				$notification,
				0,
				$e
			);
		}
	}
	
	/**
	 * 
	 * @param Notification $notification
	 * @throws \Exception
	 * @throws CantDeleteException
	 */
	public function deleteFilters(Notification $notification) {
		$filters = $notification->notificationFilters;
		if (count($filters) > 0) {
			foreach ($filters as $filter) {
				try {
					if (!$filter->delete()) {
						throw new CantDeleteException(
							'Не удалось открепить фильтр с идентификатором ' . $filter->filter_id
						);
					}
				} catch (\Exception $e) {
					throw $e;
				}
			}
		}
	}
	
	/**
	 * 
	 * @param Notification $notification
	 * @param type $filter_id
	 * @return integer $notification_filter_id
	 * @throws CantSaveException
	 */
	public function addFilter(Notification $notification, $filter_id) {
		$filter = NotificationFilter::findOne([
			'notification_id' => $notification->id,
			'filter_id' => $filter_id
		]);
		if ($filter === null) {
			$filter = new NotificationFilter();
			$filter->notification_id = $notification->id;
			$filter->filter_id = $filter_id;
		}
		
		try {
			if (!$filter->save()) {
				throw new CantSaveException('Не удалось добавить фильтр с идентификатором ' . $filter_id);
			}
		} catch (\Exception $e) {
			throw new CantSaveException(
				'Не удалось добавить фильтр с идентификатором ' . $filter_id,
				$notification,
				0,
				$e
			);
		}
		return $filter->id;
	}
	
}
