<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\services;

use Yii;

use domain\entities\Contact\Contact;
use domain\forms\Contact\ContactForm;
use domain\repositories\ContactRepository;
use domain\repositories\FilterRepository;

/**
 * Description of ContactService
 *
 * @author Rust
 */
class ContactService {
	
	private $contactRepository;
	private $filterRepository;
	
	/**
	 * 
	 * @param ContactRepository $contactRepository
	 * @param FilterRepository $filterRepository
	 */
	public function __construct(ContactRepository $contactRepository, FilterRepository $filterRepository) {
		$this->contactRepository = $contactRepository;
		$this->filterRepository = $filterRepository;
	}
	
	/**
	 * 
	 * @param integer $id
	 * @return ContactForm
	 */
	public function findById($id) {
		$contact = $this->contactRepository->get($id);
		return new ContactForm($contact);
	}
	
	/**
	 * 
	 * @param ContactForm $form
	 * @return integer $id
	 */
	public function create(ContactForm $form) {
		$contact = new Contact($form->addressname, $form->client_name);
		$this->populate($contact, $form);
		$this->filterRepository->ensureExists($form->filters);
		return $this->transactSave($contact, $form->filters);
	}
	
	/**
	 * 
	 * @param ContactForm $form
	 * @return integer $id
	 */
	public function edit(ContactForm $form) {
		$contact = $this->contactRepository->get($form->id);
		$this->populate($contact, $form);
		$this->filterRepository->ensureExists($form->filters);
		return $this->transactSave($contact, $form->filters);
	}
	
	/**
	 * 
	 * @param integer $id
	 */
	public function deleteById($id) {
		$contact = $this->contactRepository->get($id);
		$this->contactRepository->delete($filter);
	}
	
	/**
	 * 
	 * @param Contact $contact
	 * @param array $filters
	 * @return integer $id
	 * @throws \Exception
	 */
	protected function transactSave(Contact $contact, $filters = []) {
		$transaction = Yii::$app->db->beginTransaction();
		$id = null;
		try {
			$this->contactRepository->deleteFilters($contact);
			$id = $this->contactRepository->save($contact);
			if (count($filters) > 0) {
				foreach ($filters as $key => $filter_id) {
					$this->contactRepository->addFilter($contact, $filter_id);
				}
			}
			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
		return $id;
	}
	
	/**
	 * 
	 * @param Contact $contact
	 * @param ContactForm $form
	 */
	protected function populate(Contact &$contact, ContactForm $form) {
		$contact->addressname = $form->addressname;
		$contact->client_name = $form->client_name;
		$contact->phone = $form->phone;
		$contact->email = $form->email;
		$contact->sigment1 = $form->sigment1;
		$contact->sigment2 = $form->sigment2;
	}
	
}
