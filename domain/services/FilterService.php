<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\services;


use domain\repositories\FilterRepository;
use domain\forms\FilterForm;
use domain\entities\Filter\Filter;
use domain\exceptions\NotFoundException;

/**
 * Description of FilterService
 *
 * @author Rust
 */
class FilterService {
	
	public $repository;
	
	/**
	 * 
	 * @param FilterRepository $filterRepository
	 */
	public function __construct(FilterRepository $filterRepository) {
		$this->repository = $filterRepository;
	}
	
	/**
	 * 
	 * @param type $id
	 * @return FilterForm
	 */
	public function findById($id) {
		$filter = $this->repository->get($id);
		return new FilterForm($filter);
	}
	
	/**
	 * 
	 * @return Filter[]
	 */
	public function all() {
		return $this->repository->getAll();
	}
	
	/**
	 * 
	 * @param FilterForm $form
	 * @return integer $id
	 */
	public function create(FilterForm $form) {
		$filter = new Filter($form->title, $form->description);
		return $this->repository->save($filter);
	}
	
	/**
	 * 
	 * @param FilterForm $form
	 * @return integer $id
	 * @throws NotFoundException
	 */
	public function edit(FilterForm $form) {
		$filter = $this->repository->get($form->id);
		$filter->title = $form->title;
		$filter->description = $form->description;
		return $this->repository->save($filter);
	}
	
	public function deleteById($id) {
		$filter = $this->repository->get($id);
		$this->repository->delete($filter);
	}
	
}
