<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\services;

use yii\helpers\ArrayHelper;

use domain\services\mailing\SmsService;
use domain\services\mailing\EmailService;
use domain\entities\Notification\Notification;
use domain\repositories\NotificationRepository;
use domain\repositories\ContactRepository;
use domain\repositories\FilterRepository;

/**
 * Description of NotificationDispatcher
 *
 * @author Rust
 */
class NotificationDispatcher {
	
	private $notificationRepository;
	private $contactRepository;
	private $filterRepository;
	private $emailService;
	private $smsService;
	
	/**
	 * 
	 * @param NotificationRepository $notificationRepository
	 * @param ContactRepository $contactRepository
	 * @param FilterRepository $filterRepository
	 * @param EmailService $emailService
	 * @param SmsService $smsService
	 */
	public function __construct(
			NotificationRepository $notificationRepository, 
			ContactRepository $contactRepository,
			FilterRepository $filterRepository,
			EmailService $emailService,
			SmsService $smsService
	) {
		$this->notificationRepository = $notificationRepository;
		$this->contactRepository = $contactRepository;
		$this->filterRepository = $filterRepository;
		$this->emailService = $emailService;
		$this->smsService = $smsService;
	}
	
	/**
	 * 
	 * @param integer $id
	 */
	public function notifyById($id) {
		$notification = $this->notificationRepository->get($id);
		$this->notify($notification);
	}
	
	/**
	 * 
	 * @param string $filterTitle
	 */
	public function notifyByFilterTitle(string $filterTitle) {
		$filter = $this->filterRepository->getByTitle($filterName);
		$notifications = $this->notificationRepository->getByFilterId($filter->id);
		$this->notifyAll($notifications);
	}
	
	/**
	 * 
	 * @param string[] $filterTitles
	 */
	public function notifyByFilterTitles(array $filterTitles) {
		$filters = $this->filterRepository->getByTitles($filterTitles);
		if (count($filters) > 0) {
			foreach ($filters as $filter) {
				$notifications = $this->notificationRepository->getByFilterId($filter->id);
				$this->notifyAll($notifications);
			}
		}
	}
	
	/**
	 * 
	 * @param string[] $titles
	 */
	public function notifyByTitles(array $titles) {
		$notifications = $this->notificationRepository->getByTitles($titles);
		$this->notifyAll($notifications);
	}
	
	/**
	 * 
	 * @param Notification[] $notifications
	 */
	protected function notifyAll(array $notifications) {
		if (count($notifications) > 0) {
			foreach ($notifications as $notification) {
				$this->notify($notification);
			}
		}
	}
	
	/**
	 * 
	 * @param Notification $notification
	 */
	protected function notify(Notification $notification) {
		$heading = $notification->heading;
		$body = $notification->content;
		$filters = $notification->notificationFilters;
		$contacts = $this->contactRepository->getByFilterIds(ArrayHelper::getColumn($filters, 'filter_id'));
		if (count($contacts) > 0) {
			foreach ($contacts as $contact) {
				if ($notification->email) {
					$this->emailService->send($contact->email, $heading, $body);
				}
				if ($notification->sms) {
					$this->smsService->send($contact->phone, $heading, $body);
				}
			}
		}
	}
	
}
