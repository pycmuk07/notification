<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\services;

use Yii;

use domain\repositories\NotificationRepository;
use domain\repositories\FilterRepository;
use domain\forms\Notification\NotificationForm;
use domain\entities\Notification\Notification;
use domain\exceptions\NotFoundException;



/**
 * Description of NotificationService
 *
 * @author Rust
 */
class NotificationService {
	
	private $notificationRepository;
	private $filterRepository;
	
	/**
	 * 
	 * @param NotificationRepository $repository
	 * @param FilterRepository $filterRepository
	 */
	public function __construct(NotificationRepository $repository, FilterRepository $filterRepository) {
		$this->notificationRepository = $repository;
		$this->filterRepository = $filterRepository;
	}
	
	/**
	 * 
	 * @param integer $id
	 * @return NotificationForm
	 */
	public function findById($id) {
		$notification = $this->notificationRepository->get($id);
		return new NotificationForm($notification);
	}
	
	/**
	 * 
	 * @param NotificationForm $form
	 * @throws \Exception | CantSaveException
	 * @return integer $id
	 */
	public function create(NotificationForm $form) {
		$notification = new Notification($form->title);
		$this->populate($notification, $form);
		$this->filterRepository->ensureExists($form->filters);
		return $this->transactSave($notification, $form->filters);
	}
	
	/**
	 * 
	 * @param NotificationForm $form
	 * @throws \Exception | CantSaveException
	 * @return integer $id
	 */
	public function edit(NotificationForm $form) {
		$notification = $this->notificationRepository->get($form->id);
		$this->populate($notification, $form);
		$this->filterRepository->ensureExists($form->filters);
		return $this->transactSave($notification, $form->filters);
	}
	
	/**
	 * 
	 * @param integer $id
	 */
	public function deleteById($id) {
		$notification = $this->notificationRepository->get($id);
		$this->notificationRepository->delete($filter);
	}
	
	/**
	 * 
	 * @param Notification $notification
	 * @param array $filters
	 * @return integer $id
	 * @throws \Exception
	 */
	protected function transactSave(Notification $notification, $filters = []) {
		$transaction = Yii::$app->db->beginTransaction();
		$id = null;
		try {
			$this->notificationRepository->deleteFilters($notification);
			$id = $this->notificationRepository->save($notification);
			if (count($filters) > 0) {
				foreach ($filters as $key => $filter_id) {
					$this->notificationRepository->addFilter($notification, $filter_id);
				}
			}
			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
		return $id;
	}
	
	/**
	 * 
	 * @param Notification $notification
	 * @param NotificationForm $form
	 */
	protected function populate(Notification &$notification, NotificationForm $form) {
		$notification->title = $form->title;
		$notification->heading = $form->heading;
		$notification->content = $form->content;
		$notification->email = $form->email;
		$notification->sms = $form->sms;
	}
	
}
