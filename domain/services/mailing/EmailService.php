<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\services\mailing;

use Yii;

/**
 * Description of EmailService
 *
 * @author Rust
 */
class EmailService extends MailingService {
	
	/**
	 * 
	 * @param string $to	- email
	 * @param string $heading
	 * @param string $body
	 */
	public function send($to, $heading, $body) {
		//Yii::$app->mailer->compose('layouts\html', ['body' => $body])
		Yii::$app->mailer->compose('views/mail', ['body' => $body])
			//->setFrom()
			->setTo($to)
			->setSubject($heading)
			//->setHtmlBody($body)
			->send();
	}

}
