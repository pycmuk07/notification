<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\services\mailing;

/**
 * Description of MailingService
 *
 * @author Rust
 */
abstract class MailingService {
	
	abstract public function send($to, $heading, $body);
	
}
