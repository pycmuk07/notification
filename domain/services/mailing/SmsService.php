<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\services\mailing;

use yii\helpers\ArrayHelper;
use app\components\sms\SMSRU;
use domain\exceptions\CantSendException;
use domain\exceptions\IncorrectTypeException;

/**
 * Description of SmsService
 *
 * @author Rust
 */
class SmsService extends MailingService {

	public $url;
	public $api;
	public $sender;

	/**
	 * 
	 * @param string $to		- номер или номера телефонов в виде строки, раздеоенных запятой
	 * @param string $heading
	 * @param string $body
	 */
	public function send($to, $heading, $body) {
		$to = explode(',', $to);
		$message = $heading . "\n" . $body;
		if (count($to) > 1) {
			$this->sendMultiple($to, $message);
			return;
		}
		$this->sendOne(ArrayHelper::getValue($to, 0), $message);
	}

	/**
	 * 
	 * @param array $to
	 * @param string $message
	 * @throws CantSendException
	 */
	protected function sendMultiple($to, $message) {
		
		$smsru = new SMSRU($this->api); // Ваш уникальный программный ключ, который можно получить на главной странице

		$data = new \stdClass();
		/* Если текст на номера один */
		$data->to = implode(',', $to); // Номера для отправки сообщения (От 1 до 100 шт за раз). Вторым указан городской номер, по которому будет возвращена ошибка
		$data->text = $message; // Текст сообщения
		/* Если текст разный. В этом случае $data->to и $data->text обрабатываться не будут и их можно убрать из кода */

		$multi = [];
		foreach ($to as $phone) {
			$multi[$phone] = $message;
		}

		$data->multi = $multi;
		// $data->from = ''; // Если у вас уже одобрен буквенный отправитель, его можно указать здесь, в противном случае будет использоваться ваш отправитель по умолчанию
		// $data->time = time() + 7*60*60; // Отложить отправку на 7 часов
		// $data->translit = 1; // Перевести все русские символы в латиницу (позволяет сэкономить на длине СМС)
		// $data->test = 1; // Позволяет выполнить запрос в тестовом режиме без реальной отправки сообщения
		// $data->partner_id = ''; // Можно указать ваш ID партнера, если вы интегрируете код в чужую систему
		$request = $smsru->send($data); // Отправка сообщений и возврат данных в переменную

		/* 
		if ($request->status != "OK") {
			throw new CantSendException($request->status_text);
		}
		 * 
		 */
	}

	/**
	 * 
	 * @param string $to
	 * @param string $message
	 * @throws CantSendException
	 * @throws IncorrectTypeException
	 */
	protected function sendOne($to, $message) {
		if ($to == null) {
			throw new IncorrectTypeException('Недопустимое значение для номера телефона');
		}
		$smsru = new SMSRU($this->api); // Ваш уникальный программный ключ, который можно получить на главной странице

		$data = new \stdClass();
		$data->to = $to;
		$data->text = $message; // Текст сообщения
		// $data->from = ''; // Если у вас уже одобрен буквенный отправитель, его можно указать здесь, в противном случае будет использоваться ваш отправитель по умолчанию
		// $data->time = time() + 7*60*60; // Отложить отправку на 7 часов
		// $data->translit = 1; // Перевести все русские символы в латиницу (позволяет сэкономить на длине СМС)
		// $data->test = 1; // Позволяет выполнить запрос в тестовом режиме без реальной отправки сообщения
		// $data->partner_id = '1'; // Можно указать ваш ID партнера, если вы интегрируете код в чужую систему
		$sms = $smsru->send_one($data); // Отправка сообщения и возврат данных в переменную

		/*
		if ($request->status != "OK") {
			throw new CantSendException($request->status_text);
		}
		 * 
		 */
	}

}
