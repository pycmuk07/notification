<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\traits;
/**
 * Description of InstantiateTrait
 *
 * @author Rust
 */
trait InstantiateTrait {
	
	private static $_instance;
	private static $_prototype;
	
	public static function instance($refresh = false) {
		if ($refresh || self::$_prototype === null) {
			self::$_prototype = self::instantiate([]);
		}
		return self::$_prototype;
	}
	
	public static function instantiate($row) {
		if (self::$_prototype === null) {
			$class = get_called_class();
			self::$_prototype = unserialize(sprintf('O:%d:"%s":0:{}', strlen($class), $class));
		}
		$entity = clone self::$_prototype;
		$entity->init();
		return $entity;
	}
	
}
