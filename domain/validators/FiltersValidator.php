<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\validators;

use yii\validators\FilterValidator;

/**
 * Description of FiltersValidator
 *
 * @author Rust
 */
class FiltersValidator extends FilterValidator {
	
	public function init() {
		$this->filter = function($val) {
				if (is_array($val)) {
					return $val;
				}
				if (trim($val) !== '') {
					return [$val];
				}
				return [];
			};
		parent::init();
	}
	
}
