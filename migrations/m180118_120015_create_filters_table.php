<?php

use yii\db\Migration;

/**
 * Handles the creation of table `filters`.
 */
class m180118_120015_create_filters_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('filters', [
            'id' => $this->primaryKey(),
			'title' => $this->string()->notNull()->comment('Название фильтра'),
			'description' => $this->text()->comment('Описание')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('filters');
    }
}
