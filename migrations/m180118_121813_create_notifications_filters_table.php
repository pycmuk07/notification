<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notifications_filters`.
 */
class m180118_121813_create_notifications_filters_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('notifications_filters', [
            'id' => $this->primaryKey(),
			'notification_id' => $this->integer()->notNull(),
			'filter_id' => $this->integer()->notNull()
        ]);
		
		$this->addForeignKey('fk_notifs_filters_notifications', 'notifications_filters', 'notification_id', 'notifications', 'id', 'CASCADE', 'RESTRICT');
		$this->addForeignKey('fk_notifs_filters_filters', 'notifications_filters', 'filter_id', 'filters', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
		$this->dropForeignKey('fk_notifs_filters_notifications', 'notifications_filters');
		$this->dropForeignKey('fk_notifs_filters_filters', 'notifications_filters');
        $this->dropTable('notifications_filters');
    }
}
