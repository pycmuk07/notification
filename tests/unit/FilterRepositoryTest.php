<?php

use domain\repositories\FilterRepository;
use domain\entities\Filter\Filter;
use domain\exceptions\CantSaveException;

class FilterRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
	protected $repository;
	protected $filter;

	protected function _before()
    {
		$this->repository = new FilterRepository;
		$filter = new Filter('test filter', 'test description');
		$filter->save();
		$this->filter = $filter;
    }

    protected function _after()
    {
		$this->filter->delete();
    }

    
    public function testGetFilter()
    {
		$filter = $this->repository->get($this->filter->id);
		$this->assertInstanceOf(Filter::class, $filter);
		
		$this->assertEquals($this->filter->id, $filter->id);
		$this->assertEquals($this->filter->title, $filter->title);
		$this->assertEquals($this->filter->description, $filter->description);
    }
	
	public function testSaveFilter() {
		$filter = new Filter('new filter title', 'new filter description');
		$this->tester->assertGreaterThan(0, $this->repository->save($filter));
		$filter->delete();
	}
	
	public function testCantSaveException() {
		$filter = new Filter('', 'cant save filter description');
		$filter->title = null;
		$this->tester->expectException(new CantSaveException('Не удалось сохранить фильтр'), function() use ($filter) {
			$this->repository->save($filter);
		});
		$filter->delete();
	}
	
}