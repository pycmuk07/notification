<?php

use domain\forms\Notification\NotificationForm;

class NotificationFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testFiltersValidation()
    {
		$form = new NotificationForm();
		$form->title = 'test';
		
		$form->filters = '';
		$form->validate();
		$this->assertEquals([], $form->filters);
		
		$form->filters = '1';
		$form->validate();
		$this->assertEquals(['1'], $form->filters);
    }
}