<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model domain\entities\Contact\ContactFrom */
/* @var $form yii\widgets\ActiveForm */

$error = null;
if (Yii::$app->session->hasFlash('domainError')) {
	$error = Yii::$app->session->getFlash('domainError');
}

?>

<div class="contact-form">

	<?php
		if ($error !== null) {
			echo Alert::widget([
				'options' => [
					'class' => 'alert-danger'
				],
				'body' => $error->getMessage()
			]);
		}
	?>
	
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'addressname')->textInput() ?>

    <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sigment1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sigment2')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'filters')->widget(Select2::class, [
		'options' => [
			'placeholder' => 'Выберите один или несколько фильтров',
			'multiple' => true
		],
		'data' => $filters,
		'pluginOptions' => [
			'allowClear' => true,
		]
	]) ?>
	
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
