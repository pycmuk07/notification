<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model domain\entities\Contact\Contact */

$this->title = 'Create Contact';
$this->params['breadcrumbs'][] = ['label' => 'Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'filters' => $filters
    ]) ?>

</div>
