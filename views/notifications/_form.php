<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

use kartik\select2\Select2;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model domain\entities\Notification\NotificationForm */
/* @var $form yii\widgets\ActiveForm */

$error = null;
if (Yii::$app->session->hasFlash('domainError')) {
	$error = Yii::$app->session->getFlash('domainError');
}

?>

<div class="notification-form">

	<?php
		if ($error !== null) {
			echo Alert::widget([
				'options' => [
					'class' => 'alert-danger'
				],
				'body' => $error->getMessage()
			]);
		}
	?>
	
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'heading')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(Widget::class, [
		'settings' => [
			'lang' => 'ru',
			'min-height' => 200,
			'fileUpload' => Url::to(['/notifications/file-upload']),
			'imageUpload' => Url::to(['/notifications/file-upload']),
			'plugins' => [
				'clips',
				'fullscreen',
				'filemanager',
				'imagemanager'
			]
		]
	]) ?>

    <?= $form->field($model, 'email')->checkbox() ?>

    <?= $form->field($model, 'sms')->checkbox() ?>

	<?= $form->field($model, 'filters')->widget(Select2::class, [
		'options' => [
			'placeholder' => 'Выберите один или несколько фильтров',
			'multiple' => true
		],
		'data' => $filters,
		'pluginOptions' => [
			'allowClear' => true,
		]
	]) ?>
	
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
