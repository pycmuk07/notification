<?php

use yii\helpers\Html;
use yii\grid\GridView;
use domain\widgets\NotificationAlert;

/* @var $this yii\web\View */
/* @var $searchModel domain\entities\Notification\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Notification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	
	<?= NotificationAlert::widget(); ?>
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'heading',
            'content:ntext',
            'email:boolean',
            'sms:boolean',
			[
				'format' => 'raw',
				'value' => function($model) {
					return Html::a('Разослать', ['send', 'id' => $model->id], ['class' => 'btn btn-info']);
				}
			],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
